<?php

namespace App;
use App\User;
use App\Archivo;
use Illuminate\Database\Eloquent\Model;


class Registro_multimedia extends Model
{

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
       'description', 'title', 'date','users_id',

   ];

   public function user(){
    return $this->belongsTo(User::class);
}

public function archi(){
    return $this->hasOne(Archivo::class);
}

}
