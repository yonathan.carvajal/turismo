<?php

use App\Archivo;
use Illuminate\Database\Seeder;

class ArchivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cantidadArchivos=10;
        factory(Archivo::class, $cantidadArchivos)->create();
    }
}
