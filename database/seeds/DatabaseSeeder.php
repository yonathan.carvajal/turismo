<?php

use App\User;
use App\Registro_multimedia;
use App\Archivo;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        User::truncate();
        Registro_multimedia::truncate();
        Archivo::truncate();
        $this->call(UserSeeder::class);
        $this->call(RegistrosSeeder::class);
        $this->call(ArchivosSeeder::class);
           
    }
}
